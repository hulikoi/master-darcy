package Servlets;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class UserDAO {
    Properties dbProps;
    ServletContext context;
    //constructor p
    public UserDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*todo make methods to connect to the db, get the user details for a login number.
    might need a separate method to check if a user exists first, return true or false, then if true, continue to get the user details, if false then tell servlet to re-direct to the login page and the login page displays message.
    todo also need a method to connect to db and create a new entry.
    */

    //create a user object from the db info adn return that user to calling method.
    public User getUser(String id) {
        User user = null;
        System.out.println("getUser method accessed");

        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM user_details WHERE user_id =?")){
                Stmt.setInt(1, Integer.parseInt(id));
                //send query and get the results
                try (ResultSet rs=Stmt.executeQuery()){
                    //create user, assuming columns are in order id, name,location, img src string
                    while (rs.next()) {
                        String name = rs.getString(2);
                        String loc = rs.getString(3);
                        String img = rs.getString(4);

                        User nuser = new User(Integer.parseInt(id), name, loc, img);
                        return nuser;
                    }
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return user;
    }

    public void addUser(User user){
        //Use 'getUser' to check and get if any existing user in the db.
        // If so, we do not go on
        User existingUser= getUser(user.getId()+"");
        if(existingUser!=null){
            return;
        }

        //get db connection
        // try to add new user
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO user_details (name,location,img_src) VALUES (?,?,?)")){
                Stmt.setString(1,user.getName());
                Stmt.setString(2,user.getLocation());
                Stmt.setString(3,user.getUrl());

                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }

    public void addMemory(User user, String memory){

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO corny_strings (user_id,input) VALUES (?,?)")){
                Stmt.setInt(1,user.getId());
                Stmt.setString(2,memory);


                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }

    public void addPicURL(User user){
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for pic transfer");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE user_details SET img_src =? WHERE user_id = ?;")){
                Stmt.setString(1,user.getUrl());
                Stmt.setInt(2,user.getId());

                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }



}
